package ru.edu.helpdesk.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.edu.helpdesk.entity.Comment;
import ru.edu.helpdesk.entity.Ticket;
import ru.edu.helpdesk.entity.User;
import ru.edu.helpdesk.entity.UserRole;
import ru.edu.helpdesk.security.HelpdeskUserPrincipal;
import ru.edu.helpdesk.service.CommentServiceImpl;
import ru.edu.helpdesk.service.TicketServiceImpl;

import java.util.List;

/**
 * Контроллер для пользователя user
 * Главное меню, просмотр всех своих заявок, информация о конкретной заявке, создание новой заявки
 */
@Slf4j
@Controller
@Secured("ROLE_USER")
public class TicketController {

    private final TicketServiceImpl ticketService;
    private final CommentServiceImpl commentService;

    @Autowired
    public TicketController(TicketServiceImpl ticketService, CommentServiceImpl commentService) {
        this.ticketService = ticketService;
        this.commentService = commentService;
    }


    /**
     * Get запрос на главную страницу приложения после аутентификации.
     * Авторизованный пользователь видит список только своих заявок и имеет доступ ко всем сервисам
     *
     * @param model, principal
     * @return ticket.html
     */

    @GetMapping("/ticket")
    public String ticketView(Model model, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        if (principal != null) {
            final User current = principal.getUser();
            final List<Ticket> tickets;
            tickets = ticketService.allTicketsByClientId(current.getId());
            model.addAttribute("current", current);
            model.addAttribute("tickets", tickets);
            log.info("GET '/ticket'");
            return "ticket/ticket";
        } else {
            log.error("GET '/ticket' - ошибка доступа!");
            return "error/error";
        }
    }

    /**
     * Get запрос на страницу с подробной информацией о конкретной заявке.
     *
     * @param id, model, principal
     * @return ticketInfo.html
     */
    @GetMapping("/ticket/{id}")
    public String ticketInfo(@PathVariable("id") long id, Model model, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        if (principal != null) {
            final User current = principal.getUser();
            model.addAttribute("current", current);
            model.addAttribute("ticketInfo", ticketService.ticketInfo(id));
            model.addAttribute("messages", commentService.allMessageByTicketId(id));
            model.addAttribute("comment", new Comment());
            log.info("GET '/ticket/" + id + "'");
            return "ticket/ticketInfo";
        } else {
            log.error("GET '/ticket/" + id + "' - ошибка доступа!");
            return "error/error";
        }

    }

    /**
     * Get запрос на страницу создания нового обращения.
     *
     * @param model, principal
     * @return newTicket.html
     */
    @GetMapping("/ticket/new")
    public String ticketNew(Model model, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        if (principal != null) {
            final User current = principal.getUser();
            model.addAttribute("current", current);
            model.addAttribute("ticket", new Ticket());
            model.addAttribute("titles", ticketService.allTitles());
            log.info("GET '/ticket/new'");
            return "ticket/newTicket";
        } else {
            log.error("GET '/ticket/new' - ошибка доступа!");
            return "error/error";
        }
    }

    /**
     * Post запрос - создание в базе данных (таблице ticket) нового обращения и возврат в главное меню
     *
     * @param ticket, principal
     * @return ticket.html
     */
    @PostMapping("/ticket/new")
    public String createTicket(@ModelAttribute Ticket ticket, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        if (principal != null) {
            final User current = principal.getUser();
            ticket.setClient(current);
            ticketService.createTicket(ticket);
            log.info("POST '/ticket/new'");
            return "redirect:/ticket";
        } else {
            log.error("POST '/ticket/new' - ошибка доступа!");
            return "error/error";
        }
    }

    /**
     * Post запрос - создание в базе данных (таблице comment) нового комментария и перзагрузка страницы со всеми комментариями
     *
     * @param id, comment, principal
     */
    @PostMapping("/ticket/{id}")
    public String createComment(@PathVariable("id") long id, @ModelAttribute Comment comment, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        if (principal != null) {
            final User current = principal.getUser();
            comment.setTicket(ticketService.ticketInfo(id));
            comment.setUser(current);
            comment.setId(null);
            commentService.createComment(comment);
            log.info("POST '/ticket/" + id + "'");

            return "redirect:/ticket/" + id;
        }
        log.error("POST '/ticket/" + id + "' - ошибка доступа!");
        return "error/error";

    }
}