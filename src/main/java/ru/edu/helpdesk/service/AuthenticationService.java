package ru.edu.helpdesk.service;

import ru.edu.helpdesk.entity.User;

public interface AuthenticationService {

    void saveUser(User user);

    User getUserByUsername(String username);
}
