package ru.edu.helpdesk.form;

import lombok.Data;
import ru.edu.helpdesk.entity.UserRole;

@Data
public class UserEditForm {
    private Long id;
    private String login;
    private String firstName;
    private String lastName;
    private UserRole role;
    private Boolean changePassword;
    private String newPassword;
}
