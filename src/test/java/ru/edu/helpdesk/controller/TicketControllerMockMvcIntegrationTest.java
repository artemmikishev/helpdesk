package ru.edu.helpdesk.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Интеграционный тест для TicketController
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TicketControllerMockMvcIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    /**
     * Get запрос на /ticket
     * успешный тест с ролью USER
     * так как доступ к /ticket есть только у роли USER
     */
    @Test
    @WithUserDetails("user1")
    public void ticketViewSuccessTest() throws Exception {
        this.mockMvc.perform(get("/ticket"))
                .andExpect(status().isOk());

    }

    /**
     * Get запрос на /ticket
     * тест c анонимным пользователем, происходит перенаправление на страницу /login
     */
    @Test
    public void ticketViewNegativeTest() throws Exception {
        this.mockMvc.perform(get("/ticket"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://localhost/login"));

    }

    /**
     * Проверка формы авторизации - успешный сценарий
     * В базе данных есть пользователь с ролью USER (логин user1, пароль user1)
     * после успешной авторизации происходит перенаправление на главную страницу /ticket
     */
    @Test
    public void loginFormTest() throws Exception {
        this.mockMvc.perform(formLogin().user("user1").password("user1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/ticket"));

    }

    /**
     * Проверка формы авторизации - негативный сценарий
     * В базе данных нет пользователя с такими параметрами
     */
    @Test
    public void loginNegativeTest() throws Exception {
        this.mockMvc.perform(post("/login").param("test", "test"))
                .andExpect(status().isForbidden());


    }

    /**
     * Get запрос на /ticket/{id}
     * успешный тест с ролью USER
     */
    @Test
    @WithUserDetails("user1")
    public void ticketInfoSuccessTest() throws Exception {
        this.mockMvc.perform(get("/ticket/{id}", 9L))
                .andExpect(status().isOk());

    }

    /**
     * Get запрос на /ticket/new
     * успешный тест с ролью USER
     */
    @Test
    @WithUserDetails("user1")
    public void ticketNewSuccessTest() throws Exception {
        this.mockMvc.perform(get("/ticket/new"))
                .andExpect(status().isOk());

    }

}

